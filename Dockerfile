FROM rust:slim

RUN set -eux; \
    apt-get update; \
    apt-get install -y \
        binutils-arm-linux-gnueabihf \
        gcc-arm-linux-gnueabihf \
        musl-tools \
        git \
        ; \
    ln -s /usr/bin/arm-linux-gnueabihf-gcc /usr/bin/arm-linux-musleabihf-gcc; \
    rustup target add arm-unknown-linux-gnueabihf; \
    git clone --depth=1 https://github.com/raspberrypi/tools rpi_tools; \
    apt-get remove -y --auto-remove \
        git \
        ; \
    rm -rf /var/lib/apt/lists/*;

ENV RUSTFLAGS="-C linker=$HOME/rpi_tools/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/bin/arm-linux-gnueabihf-gcc"
