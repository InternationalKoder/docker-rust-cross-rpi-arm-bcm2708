# Repository content

This repository contains files that can be used to build a Docker image containing necessary tools to build [Rust](https://www.rust-lang.org) projects targeting the ARM BCM2708/BCM2835 chip.

The Docker image is built automatically once a month.

# About the Docker image

The image is made for the purpose of cross compiling Rust code from a x86_64 system targeting the **first generation** [Raspberry Pi](https://www.raspberrypi.com/) computers, which use the BCM2708 chip.

It can be pulled from `registry.gitlab.com/internationalkoder/docker-rust-cross-rpi-arm-bcm2708:latest`.

[Rust official Docker image](https://registry.hub.docker.com/_/rust) variant `slim` is used as the source to build this image. Thus glibc should be used to build Rust projects.
